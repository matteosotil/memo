package org.msb.games.memo.model.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MemoCardImplTest {
	private MemoCardImpl memoCardImpl;

	@Test
	public void testConstructor_byImage() {
		final String image = "anImage";
		memoCardImpl = new MemoCardImpl(image);
		Assert.assertTrue(
				!memoCardImpl.isDiscovered() && !memoCardImpl.isShown() && image.equals(memoCardImpl.getImage()));
	}

	// EQUALS

	@Test
	public void testEquals_null() {
		Assert.assertFalse(new MemoCardImpl("anImage").equals(null));
	}

	@Test
	public void testEquals_differentClass() {
		Assert.assertFalse(new MemoCardImpl("anImage").equals(new Object()));
	}

	@Test
	public void testEquals_differentImage() {
		Assert.assertFalse(new MemoCardImpl("anImage").equals(new MemoCardImpl("anotherImage")));
	}

	@Test
	public void testEquals_sameInstance() {
		MemoCardImpl card = new MemoCardImpl("anImage");
		Assert.assertTrue(card.equals(card) && card.hashCode() == card.hashCode());
	}

	@Test
	public void testEquals_sameImage() {
		MemoCardImpl firstCard = new MemoCardImpl("anImage");
		MemoCardImpl secondCard = new MemoCardImpl("anImage");
		firstCard.setDiscovered(true);
		firstCard.show();
		secondCard.setDiscovered(false);
		secondCard.hide();
		Assert.assertTrue(firstCard.equals(secondCard) && firstCard.hashCode() == secondCard.hashCode());
	}

	@Test
	public void testEquals_nullBothImage() {
		MemoCardImpl firstCard = new MemoCardImpl(null);
		MemoCardImpl secondCard = new MemoCardImpl(null);
		firstCard.setDiscovered(true);
		firstCard.show();
		secondCard.setDiscovered(false);
		secondCard.hide();
		Assert.assertTrue(firstCard.equals(secondCard) && firstCard.hashCode() == secondCard.hashCode());
	}

	@Test
	public void testEquals_nullOneImage() {
		MemoCardImpl firstCard = new MemoCardImpl(null);
		MemoCardImpl secondCard = new MemoCardImpl("anImage");
		firstCard.setDiscovered(true);
		firstCard.show();
		secondCard.setDiscovered(false);
		secondCard.hide();
		Assert.assertFalse(firstCard.equals(secondCard));
	}
}
