package org.msb.games.memo.model.impl;

import org.junit.Assert;
import org.junit.Test;

public class MemoMoveResultImplTest {

	@Test
	public void testIsCompleted_succeededNotCompleted() throws Exception {
		MemoMoveResultImpl moveResult = new MemoMoveResultImpl(true, false);
		Assert.assertTrue(moveResult.isSucceeded() && !moveResult.isCompleted());
	}

	@Test
	public void testIsCompleted_succeededCompleted() throws Exception {
		MemoMoveResultImpl moveResult = new MemoMoveResultImpl(true, true);
		Assert.assertTrue(moveResult.isCompleted() && moveResult.isSucceeded());
	}

	@Test
	public void testIsCompleted_notSucceededCompleted() throws Exception {
		MemoMoveResultImpl moveResult = new MemoMoveResultImpl(false, true);
		Assert.assertTrue(!moveResult.isSucceeded() && moveResult.isCompleted());
	}

	@Test
	public void testIsCompleted_notSucceededNotCompleted() throws Exception {
		MemoMoveResultImpl moveResult = new MemoMoveResultImpl(false, false);
		Assert.assertTrue(!moveResult.isCompleted() && !moveResult.isSucceeded());
	}
}
