package org.msb.games.memo.business.impl;

import static org.mockito.Mockito.when;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.msb.games.memo.business.ImagesProvider;
import org.msb.games.memo.exceptions.ImagesProviderException;
import org.msb.games.memo.model.MemoMoveResult;

public class MemoBoardArrayImplTest {

	// Constructor by images provider

	@SuppressWarnings("unchecked")
	@Test(expected = ImagesProviderException.class)
	public void testConstructorImagesProvider_imagesProviderException() throws Exception {
		ImagesProvider imagesProvider = Mockito.mock(ImagesProvider.class);
		when(imagesProvider.getImages(1)).thenThrow(ImagesProviderException.class);
		new MemoBoardArrayImpl(2, 1, imagesProvider);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorImagesProvider_nullImagesProvider() throws Exception {
		new MemoBoardArrayImpl(3, 2, (ImagesProvider) null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorImagesProvider_oddNumberOfCards() throws Exception {
		ImagesProvider imagesProvider = Mockito.mock(ImagesProvider.class);
		when(imagesProvider.getImages(2)).thenReturn(new String[] { "image1", "image2" });
		new MemoBoardArrayImpl(3, 1, imagesProvider);
	}

	@Test
	public void testConstructorImagesProvider_correct2x2() throws Exception {
		ImagesProvider imagesProvider = Mockito.mock(ImagesProvider.class);
		when(imagesProvider.getImages(2)).thenReturn(new String[] { "image1", "image2" });
		new MemoBoardArrayImpl(2, 2, imagesProvider);

	}

	// Constructor by images tests

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorImagesArray_oddNumberOfCards() throws Exception {
		new MemoBoardArrayImpl(3, 3, (String[]) null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorImagesArray_notEnoughOfImages() throws Exception {
		new MemoBoardArrayImpl(3, 2, "anImage", "anotherImage");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorImagesArray_tooMuchImages() throws Exception {
		new MemoBoardArrayImpl(2, 2, "image1", "image2", "image3");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorImagesArray_repeatedImages() throws Exception {
		new MemoBoardArrayImpl(2, 2, "image1", "image1");
	}

	@Test
	public void testConstructorImagesArray_correct2x2() throws Exception {
		new MemoBoardArrayImpl(2, 2, "image1", "image2");
	}

	// Show card tests
	@Test(expected = IllegalArgumentException.class)
	public void testShowCard_wrongRow() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 2, "image1", "image2");
		board.showCard(2, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testShowCard_wrongColumn() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 2, "image1", "image2");
		board.showCard(1, 2);
	}

	@Test
	public void testShowCard_ok() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 2, "image1", "image2");
		board.showCard(1, 1);
	}

	@Test
	public void testShowCard_moreThanTwo() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 2, "image1", "image2");
		board.showCard(1, 1);
		board.showCard(0, 0);
		board.showCard(0, 1);
	}

	// Check move tests

	@Test
	public void testCheckMove_nothingShown() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 2, "image1", "image2");
		Assert.assertFalse(!board.checkMove().isCompleted() && board.checkMove().isSucceeded());
	}

	@Test
	public void testCheckMove_oneShown() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 2, "image1", "image2");
		board.showCard(0, 0);
		Assert.assertFalse(!board.checkMove().isCompleted() && board.checkMove().isSucceeded());
	}

	@Test
	public void testCheckMove_sureGuess() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 1, "image1");
		MemoMoveResult moveResult;
		board.showCard(0, 0);
		board.showCard(1, 0);
		moveResult = board.checkMove();
		Assert.assertTrue(moveResult.isCompleted() && moveResult.isSucceeded() && board.getCard(0, 0).isShown()
				&& board.getCard(1, 0).isShown());
	}

	// Refresh tests

	@Test
	public void testCheckRefresh_sureGuess() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 1, "image1");
		MemoMoveResult moveResult;
		board.showCard(0, 0);
		board.showCard(1, 0);
		moveResult = board.refresh();
		Assert.assertTrue(moveResult.isCompleted() && moveResult.isSucceeded() && board.getCard(0, 0).isShown()
				&& board.getCard(1, 0).isShown() && board.getCard(0, 0).isDiscovered()
				&& board.getCard(1, 0).isDiscovered());
	}

	// Get card tests
	@Test(expected = IllegalArgumentException.class)
	public void testGetCard_wrongRow() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 2, "image1", "image2");
		board.getCard(2, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetCard_wrongColumn() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 2, "image1", "image2");
		board.getCard(1, 3);
	}

	@Test
	public void testGetCard_ok() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 2, "image1", "image2");
		board.getCard(1, 1);
	}

	@Test
	public void testGetRowsNumber() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(3, 2, "image1", "image2", "image3");
		Assert.assertTrue(board.getRowsNumber() == 3);
	}

	@Test
	public void testGetColumnsNumber() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(3, 2, "image1", "image2", "image3");
		Assert.assertTrue(board.getColumnsNumber() == 2);
	}

	@Test
	public void testGetNumberOfCards() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(3, 2, "image1", "image2", "image3");
		Assert.assertTrue(board.getNumberOfCards() == 6);
	}

	// Is solved tests
	@Test
	public void testIsSolved_solved() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 1, "image1");
		board.showCard(0, 0);
		board.showCard(1, 0);
		board.refresh();
		Assert.assertTrue(board.isSolved());
	}

	@Test
	public void testIsSolved_notSolved() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 3, "image1", "image2", "image3");
		board.showCard(0, 0);
		board.showCard(1, 0);
		Assert.assertFalse(board.isSolved());
	}

	// Get number of cards in move

	@Test
	public void getNumberOfCardsInPlay_none() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 3, "image1", "image2", "image3");
		Assert.assertTrue(board.getNumberOfCardsInPlay() == 0);
	}

	@Test
	public void getNumberOfCardsInPlay_one() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 3, "image1", "image2", "image3");
		board.showCard(0, 0);
		Assert.assertTrue(board.getNumberOfCardsInPlay() == 1);
	}

	@Test
	public void getNumberOfCardsInPlay_two() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 3, "image1", "image2", "image3");
		board.showCard(0, 0);
		board.showCard(1, 0);
		Assert.assertTrue(board.getNumberOfCardsInPlay() == 2);
	}

	@Test
	public void getNumberOfCardsInPlay_afterRefresh() throws Exception {
		MemoBoardArrayImpl board = new MemoBoardArrayImpl(2, 3, "image1", "image2", "image3");
		board.showCard(0, 0);
		board.showCard(1, 0);
		board.refresh();
		Assert.assertTrue(board.getNumberOfCardsInPlay() == 0);
	}
}
