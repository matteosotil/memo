package org.msb.games.model;

import java.io.Serializable;

/**
 * Card for any game.
 * 
 * @author MSB
 *
 */
public interface Card extends Serializable {
	/**
	 * Shows this card.
	 */
	Card show();

	/**
	 * Hides this card.
	 */
	Card hide();

	/**
	 * Checks if this card is shown.
	 * 
	 * @return
	 */
	boolean isShown();

	/**
	 * Gets image path to be displayed when card is shown.
	 * 
	 * @return
	 */
	String getImage();
}
