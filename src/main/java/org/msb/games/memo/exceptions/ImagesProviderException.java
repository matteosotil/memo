package org.msb.games.memo.exceptions;

/**
 * Exception thrown by image providers.
 * 
 * @author msb
 *
 */
public class ImagesProviderException extends RuntimeException {

	private static final long serialVersionUID = 6381395008231164117L;

	public ImagesProviderException() {
		super();
	}

	public ImagesProviderException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public ImagesProviderException(String message, Throwable cause) {
		super(message, cause);
	}

	public ImagesProviderException(String message) {
		super(message);
	}

	public ImagesProviderException(Throwable cause) {
		super(cause);
	}

}
