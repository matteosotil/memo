package org.msb.games.memo.model.impl;

import org.msb.games.memo.model.MemoMoveResult;

public class MemoMoveResultImpl implements MemoMoveResult {

	private boolean succeeded;
	private boolean completed;

	public MemoMoveResultImpl(boolean succeeded, boolean completed) {
		super();
		this.succeeded = succeeded;
		this.completed = completed;
	}

	@Override
	public boolean isSucceeded() {
		return succeeded;
	}

	@Override
	public boolean isCompleted() {
		return completed;
	}
}
