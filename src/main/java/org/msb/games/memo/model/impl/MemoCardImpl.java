package org.msb.games.memo.model.impl;

import org.msb.games.memo.model.MemoCard;

public class MemoCardImpl implements MemoCard {

	private static final long serialVersionUID = -9058443992216040352L;

	private boolean shown;
	private boolean discovered;
	private final String image;

	public MemoCardImpl(String image) {
		super();
		this.image = image;
		setShown(false);
		setDiscovered(false);
	}

	@Override
	public boolean isShown() {
		return shown;
	}

	@Override
	public boolean isDiscovered() {
		return discovered;
	}

	@Override
	public void setDiscovered(boolean discovered) {
		this.discovered = discovered;
	}

	@Override
	public MemoCard show() {
		setShown(true);
		return this;
	}

	@Override
	public MemoCard hide() {
		setShown(false);
		return this;
	}

	@Override
	public String getImage() {
		return image;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		return result;
	}

	/**
	 * Two cards are equal if they have the same image.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof MemoCardImpl))
			return false;
		MemoCardImpl other = (MemoCardImpl) obj;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image)) {
			return false;
		}
		return true;
	}

	private void setShown(boolean shown) {
		this.shown = shown;
	}
}
