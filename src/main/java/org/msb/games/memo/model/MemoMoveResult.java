package org.msb.games.memo.model;

public interface MemoMoveResult {

	/**
	 * Checks if last memo move was complete (two cards have been shown).
	 * 
	 * @return
	 */
	boolean isCompleted();

	/**
	 * Checks if move has succeded (shown cards have been discovered).
	 * 
	 * @return
	 */
	boolean isSucceeded();
}
