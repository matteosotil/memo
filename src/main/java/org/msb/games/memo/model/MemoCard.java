package org.msb.games.memo.model;

import org.msb.games.model.Card;

/**
 * Card for memo game.
 * 
 * @author MSB
 *
 */
public interface MemoCard extends Card {
	/**
	 * Check if card has been discovered.
	 * 
	 * @return
	 */
	boolean isDiscovered();

	/**
	 * Set if card has been discovered.
	 * 
	 * @param discovered
	 */
	void setDiscovered(boolean discovered);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.msb.games.model.Card#show()
	 */
	@Override
	MemoCard show();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.msb.games.model.Card#hide()
	 */
	@Override
	MemoCard hide();
}
