package org.msb.games.memo.front;

public enum MemoGameProperties {
	MARVEL_API_PUBLIC_KEY("marvel.api.publicKey"), MARVEL_API_PRIVATE_KEY("marvel.api.privateKey");

	private String key;

	MemoGameProperties(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}
}
