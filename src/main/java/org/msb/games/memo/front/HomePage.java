package org.msb.games.memo.front;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.msb.games.memo.business.MemoBoard;
import org.msb.games.memo.business.impl.MarvelApiImagesProviderImpl;
import org.msb.games.memo.business.impl.MemoBoardArrayImpl;
import org.msb.games.memo.front.components.MemoBoardComponent;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;

	public HomePage(final PageParameters parameters) {
		super(parameters);
		MemoBoard board;
		board = new MemoBoardArrayImpl(4, 4,
				new MarvelApiImagesProviderImpl(getMarvelApiPublicKey(), getMarvelApiPrivateKey()));
		add(new MemoBoardComponent("board", new Model<MemoBoard>(board)));
	}

	private String getMarvelApiPrivateKey() {
		return getString(MemoGameProperties.MARVEL_API_PRIVATE_KEY.getKey());
	}

	private String getMarvelApiPublicKey() {
		return getString(MemoGameProperties.MARVEL_API_PUBLIC_KEY.getKey());
	}
}
