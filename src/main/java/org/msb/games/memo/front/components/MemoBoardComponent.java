package org.msb.games.memo.front.components;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.wicket.ajax.AbstractAjaxTimerBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.list.AbstractItem;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.time.Duration;
import org.msb.games.memo.business.MemoBoard;
import org.msb.games.memo.model.MemoCard;

public class MemoBoardComponent extends Panel {

	private static final long serialVersionUID = -2800061965698677129L;
	private Collection<MemoCardComponent> refreshComponents;
	private AbstractAjaxTimerBehavior timer;

	public MemoBoardComponent(String id, IModel<MemoBoard> model) {
		super(id, model);
		addWinMessageComponent();
		add(timer);
		addRestartLink();
		refreshComponents = new ArrayList<>();
		addRows(model);
	}

	private void addRestartLink() {
		add(new BookmarkablePageLink<>("restartLink", getApplication().getHomePage()));
	}

	private void addWinMessageComponent() {
		add(new Label("winMessage", getString("win.message")) {

			private static final long serialVersionUID = 1432327144144006709L;

			@Override
			public boolean isVisible() {
				return getModelObject().isSolved();
			}
		}.setOutputMarkupPlaceholderTag(true));
		timer = new AbstractAjaxTimerBehavior(Duration.seconds(3)) {
			private static final long serialVersionUID = -675405914840014559L;

			@Override
			protected void onTimer(AjaxRequestTarget target) {
				refresh(target);
			}
		};
	}

	private void addRows(IModel<MemoBoard> model) {
		RepeatingView rows = new RepeatingView("rows", model);
		MemoBoard board = model.getObject();
		add(rows);
		for (int row = 0; row < board.getRowsNumber(); row++) {
			AbstractItem item = new AbstractItem(rows.newChildId());
			rows.add(item);
			addColumns(row, item, model);
		}
	}

	/**
	 * Refreshes board.
	 * 
	 * @param target
	 */
	private void refresh(AjaxRequestTarget target) {
		timer.stop(target);
		if (getModelObject().checkMove().isCompleted()) {
			getModelObject().refresh();
			target.add(refreshComponents.toArray(new MemoCardComponent[refreshComponents.size()]));
			refreshComponents.clear();
			if (getModelObject().isSolved()) {
				target.add(get("winMessage"));
			}
		}
	}

	private void addColumns(int row, AbstractItem item, IModel<MemoBoard> model) {
		RepeatingView columns = new RepeatingView("columns", model);
		item.add(columns);
		MemoBoard board = model.getObject();
		for (int column = 0; column < board.getColumnsNumber(); column++) {
			columns.add(new MemoCardComponent(columns.newChildId(), row, column,
					new Model<MemoCard>(board.getCard(row, column))) {

				private static final long serialVersionUID = 1232842171671352774L;

				@Override
				public void onClick(AjaxRequestTarget target, MemoCardComponent memoCardComponent) {
					MemoBoardComponent.this.onClick(target, memoCardComponent);
				}
			});
		}
	}

	/**
	 * Called when a card is clicked.
	 * 
	 * @param target
	 * @param memoCardComponent
	 */
	private void onClick(AjaxRequestTarget target, MemoCardComponent memoCardComponent) {
		refresh(target);
		refreshComponents.add(memoCardComponent);
		if (getModelObject().showCard(memoCardComponent.getRow(), memoCardComponent.getColumn()).isSucceeded()) {
			refresh(target);
		} else {
			target.add(memoCardComponent);
			timer.restart(target);
		}
	}

	public MemoBoard getModelObject() {
		return (MemoBoard) getDefaultModelObject();
	}
}
