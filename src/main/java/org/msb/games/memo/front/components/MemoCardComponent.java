package org.msb.games.memo.front.components;

import org.apache.wicket.AttributeModifier;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.image.ExternalImage;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.msb.games.memo.model.MemoCard;

public abstract class MemoCardComponent extends Panel {

	private static final long serialVersionUID = 1413599388950362822L;
	private int row;
	private int column;

	public MemoCardComponent(String id, int row, int column, Model<MemoCard> model) {
		super(id, model);
		setOutputMarkupId(true);
		this.row = row;
		this.column = column;
		addLink(model);
	}

	private void addLink(Model<MemoCard> model) {
		AjaxLink<MemoCard> link = new AjaxLink<MemoCard>("clickLink", model) {

			private static final long serialVersionUID = 1L;

			@Override
			public void onClick(AjaxRequestTarget target) {
				MemoCardComponent.this.onClick(target, MemoCardComponent.this);
			}

			@Override
			public boolean isEnabled() {
				MemoCard card = getModelObject();
				return (!card.isShown() && !card.isDiscovered());
			}
		};
		addImage(link, model);
		add(link);
	}

	private void addImage(AjaxLink<MemoCard> link, Model<MemoCard> model) {
		ExternalImage image = new ExternalImage("cardImage", new PropertyModel<>(model, "image")) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isVisible() {
				return model.getObject().isShown();
			}
		};
		link.add(image);
		image.add(new AttributeModifier("class", new IModel<String>() {

			private static final long serialVersionUID = -6975151199559638345L;

			@Override
			public String getObject() {
				return "discovered ";
			}
		}) {

			private static final long serialVersionUID = -3236568664449874087L;

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * org.apache.wicket.behavior.Behavior#isEnabled(org.apache.wicket.Component)
			 */
			@Override
			public boolean isEnabled(Component component) {
				return model.getObject().isDiscovered();
			}

		});
	}

	/**
	 * @return the row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @return the column
	 */
	public int getColumn() {
		return column;
	}

	public abstract void onClick(AjaxRequestTarget target, MemoCardComponent memoCardComponent);
}
