package org.msb.games.memo.business;

import java.io.Serializable;

import org.msb.games.memo.model.MemoCard;
import org.msb.games.memo.model.MemoMoveResult;

/**
 * Board for Memo game.
 */
public interface MemoBoard extends Serializable {

	/**
	 * Gets a card.
	 * 
	 * @param row    Row position of the card in the board.
	 * @param column Column position of the card in the board.
	 * @return The card.
	 */
	MemoCard getCard(int row, int column);

	/**
	 * Show a card and get it. If number of cards in play is already 2 (maximum),
	 * checkMove() is called first.
	 * 
	 * @param row    Row position of the card in the board.
	 * @param column Column position of the card in the board.
	 * @return Shown card
	 */
	MemoMoveResult showCard(int row, int column);

	/**
	 * Checks current move, but it doesn't refresh board, so shown cards remain
	 * shown, hidden cards remain hidden and so on.
	 * 
	 * @return
	 */
	MemoMoveResult checkMove();

	/**
	 * Checks ove and refreshes board according to last move. If two cards are in
	 * play, it checks if they are the same; if they are, it marks them as
	 * discovered; if they are not, it hides them; if board has been solved, it
	 * marks it as solved.
	 * 
	 * @return
	 */
	MemoMoveResult refresh();

	/**
	 * Gets the number of rows of this board.
	 * 
	 * @return
	 */
	int getRowsNumber();

	/**
	 * Gets the number of columns of this board.
	 * 
	 * @return
	 */
	int getColumnsNumber();

	/**
	 * Returns true if game has been solved, false otherwise.
	 * 
	 * @return
	 */
	boolean isSolved();

	/**
	 * Gets the number of cards in play (shown but not yet checked).
	 * 
	 * @return
	 */
	int getNumberOfCardsInPlay();
}
