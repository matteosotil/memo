package org.msb.games.memo.business;

import org.msb.games.memo.exceptions.ImagesProviderException;

public interface ImagesProvider {
	String[] getImages(int number) throws ImagesProviderException;
}
