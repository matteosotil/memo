package org.msb.games.memo.business.impl;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.msb.games.memo.business.ImagesProvider;
import org.msb.games.memo.business.MemoBoard;
import org.msb.games.memo.exceptions.ImagesProviderException;
import org.msb.games.memo.model.MemoCard;
import org.msb.games.memo.model.MemoMoveResult;
import org.msb.games.memo.model.impl.MemoCardImpl;
import org.msb.games.memo.model.impl.MemoMoveResultImpl;

public class MemoBoardArrayImpl implements MemoBoard {
	private static final long serialVersionUID = -2025752646052516952L;
	private final MemoCard[][] board;
	private final int rowsNumber;
	private final int columnsNumber;
	private int numberOfDiscoveredCards;
	private final Deque<MemoCard> cardsInPlay;

	public MemoBoardArrayImpl(int rows, int columns, ImagesProvider imagesProvider) throws ImagesProviderException {
		this(rows, columns);
		validateImagesProvider(imagesProvider);
		String[] images = imagesProvider.getImages(getNumberOfCards() / 2);

		fillBoard(images);
	}

	public MemoBoardArrayImpl(int rows, int columns, String... images) {
		this(rows, columns);
		fillBoard(images);
	}

	private MemoBoardArrayImpl(int rows, int columns) {
		super();
		validateBoardSize(rows, columns);
		this.rowsNumber = rows;
		this.columnsNumber = columns;
		this.numberOfDiscoveredCards = 0;
		cardsInPlay = new ArrayDeque<>(2);
		board = new MemoCard[rows][columns];
	}

	@Override
	public MemoMoveResult showCard(int row, int column) {
		validatePosition(row, column);
		synchronized (this) {
			MemoMoveResult result;
			if (cardsInPlay.offer(board[row][column])) {
				board[row][column].show();
			}
			result = checkMove();
			if (result.isSucceeded()) {
				numberOfDiscoveredCards = numberOfDiscoveredCards + 2;
			}
			return checkMove();
		}
	}

	@Override
	public MemoMoveResult checkMove() {
		boolean discovered = false;
		boolean complete = false;
		synchronized (this) {
			if (getNumberOfCardsInPlay() == 2) {
				MemoCard firstCard = cardsInPlay.getFirst();
				MemoCard secondCard = cardsInPlay.getLast();
				if (firstCard.equals(secondCard)) {
					discovered = true;
				}
				complete = true;
			}
		}
		return new MemoMoveResultImpl(discovered, complete);
	}

	@Override
	public synchronized MemoMoveResult refresh() {
		MemoMoveResult moveResult = checkMove();
		if (moveResult.isCompleted()) {
			MemoCard firstCard = cardsInPlay.poll();
			MemoCard secondCard = cardsInPlay.poll();
			if (moveResult.isSucceeded()) {
				firstCard.setDiscovered(true);
				secondCard.setDiscovered(true);
			} else {
				firstCard.hide();
				secondCard.hide();
			}
		}
		return moveResult;
	}

	@Override
	public MemoCard getCard(int row, int column) {
		validatePosition(row, column);
		return board[row][column];
	}

	@Override
	public int getRowsNumber() {
		return rowsNumber;
	}

	@Override
	public int getColumnsNumber() {
		return columnsNumber;
	}

	@Override
	public boolean isSolved() {
		return getNumberOfDiscoveredCards() == getNumberOfCards();
	}

	@Override
	public int getNumberOfCardsInPlay() {
		return cardsInPlay.size();
	}

	public int getNumberOfCards() {
		return getRowsNumber() * getColumnsNumber();
	}

	public int getNumberOfDiscoveredCards() {
		return this.numberOfDiscoveredCards;
	}

	private void validateImages(int rows, int columns, String[] images) {
		final int numBoardCards = rows * columns;
		Set<String> mySet = new HashSet<>(Arrays.asList(images));
		if (mySet.size() != images.length) {
			throw new IllegalArgumentException("Images must be different");
		}
		if (images.length != numBoardCards / 2) {
			throw new IllegalArgumentException("Wrong number of images");
		}
	}

	private void validateImagesProvider(ImagesProvider imagesProvider) {
		if (imagesProvider == null) {
			throw new IllegalArgumentException("Images provider can't be null");
		}
	}

	private void fillBoard(String[] images) {
		validateImages(getRowsNumber(), getColumnsNumber(), images);
		List<MemoCard> cards = new ArrayList<>();
		for (int cardNumber = 0; cardNumber < getNumberOfCards(); cardNumber++) {
			cards.add(new MemoCardImpl(images[cardNumber / 2]));
		}
		Collections.shuffle(cards);
		Iterator<MemoCard> cardsIterator = cards.iterator();
		for (int row = 0; row < getRowsNumber(); row++) {
			for (int column = 0; column < getColumnsNumber(); column++) {
				board[row][column] = cardsIterator.next();
			}
		}
	}

	private void validateBoardSize(int rows, int columns) {
		final int numBoardCards = rows * columns;
		if ((numBoardCards) % 2 != 0) {
			throw new IllegalArgumentException("Number of cards must be pair");
		}
	}

	private void validatePosition(int row, int column) {
		if (row >= getRowsNumber()) {
			throw new IllegalArgumentException("Row must be less than board number of rows");
		}
		if (column >= getColumnsNumber()) {
			throw new IllegalArgumentException("Column must be less than board number of columns");
		}
	}
}
