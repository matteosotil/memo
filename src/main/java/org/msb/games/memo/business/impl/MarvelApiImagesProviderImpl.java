package org.msb.games.memo.business.impl;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.msb.games.memo.business.ImagesProvider;
import org.msb.games.memo.exceptions.ImagesProviderException;

import com.karumi.marvelapiclient.CharacterApiClient;
import com.karumi.marvelapiclient.MarvelApiConfig;
import com.karumi.marvelapiclient.MarvelApiException;
import com.karumi.marvelapiclient.model.CharacterDto;
import com.karumi.marvelapiclient.model.CharactersDto;
import com.karumi.marvelapiclient.model.CharactersQuery;
import com.karumi.marvelapiclient.model.MarvelImage.Size;
import com.karumi.marvelapiclient.model.MarvelResponse;

/**
 * Marvel images provider. It uses marvel api to get images.
 * 
 * @author msb
 *
 */
public class MarvelApiImagesProviderImpl implements ImagesProvider {

	private static ConcurrentMap<Integer, String[]> map = new ConcurrentHashMap<>();
	private CharacterApiClient client;

	public MarvelApiImagesProviderImpl(String publicKey, String privateKey) {
		super();
		MarvelApiConfig marvelApiConfig = new MarvelApiConfig.Builder(publicKey, privateKey).debug().build();
		client = new CharacterApiClient(marvelApiConfig);
	}

	@Override
	public String[] getImages(int number) {
		return map.computeIfAbsent(number, key -> generateImages(key));
	}

	private String[] generateImages(int number) {
		Set<String> images = new HashSet<>();
		int page = 0;
		Iterator<CharacterDto> iterator = getCharacters(page, number);
		while (images.size() < number) {
			if (!iterator.hasNext()) {
				iterator = getCharacters(++page, number);
			}
			CharacterDto character = iterator.next();
			String imageUrl = character.getThumbnail().getImageUrl(Size.STANDARD_LARGE);
			if (imageUrl != null && !imageUrl.contains("image_not_available")) {
				images.add(imageUrl);
			}
		}
		return images.toArray(new String[number]);
	}

	private Iterator<CharacterDto> getCharacters(int page, int number) {
		try {
			CharactersQuery spider = CharactersQuery.Builder.create().withOffset(page * number).withLimit(number)
					.build();
			MarvelResponse<CharactersDto> all;
			all = client.getAll(spider);
			return all.getResponse().getCharacters().iterator();
		} catch (MarvelApiException e) {
			throw new ImagesProviderException(e);
		}
	}

}
